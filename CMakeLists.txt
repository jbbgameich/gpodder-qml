project(gpodder)
cmake_minimum_required(VERSION 2.8.12)

# Find extra-cmake-modules
find_package(ECM 0.0.9 REQUIRED NO_MODULE)

# where to look first for cmake modules, before ${CMAKE_ROOT}/Modules/ is checked
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${ECM_MODULE_PATH} ${ECM_KDE_MODULE_DIR})

# Find packages
find_package(Qt5 REQUIRED NO_MODULE COMPONENTS Qml Quick Gui)

include(FeatureSummary)
include(ECMAddTests)
include(KDEInstallDirs)
include(KDECMakeSettings)
include(KDECompilerSettings)
include(GenerateExportHeader)
include(ECMInstallIcons)

kde_enable_exceptions()

add_subdirectory(src)

install(FILES io.thp.gpodder-qml.desktop DESTINATION ${XDG_APPS_INSTALL_DIR})
install(DIRECTORY gpodder-ui-qml/common gpodder-ui-qml/touch DESTINATION ${DATA_INSTALL_DIR}/gpodder)
install(FILES gpodder-ui-qml/main.py DESTINATION ${DATA_INSTALL_DIR}/gpodder)
ecm_install_icons(ICONS sc-apps-io.thp.gpodder-qml.svg DESTINATION ${ICON_INSTALL_DIR})

feature_summary(WHAT ALL FATAL_ON_MISSING_REQUIRED_PACKAGES)
