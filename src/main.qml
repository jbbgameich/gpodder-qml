import QtQuick 2.7
import QtQuick.Controls 2.0 as Controls
import QtQuick.Window 2.2
import org.kde.kirigami 2.0 as Kirigami
import io.thp.pyotherside 1.3
import "touch"

Kirigami.ApplicationWindow {
	visible: true
	title: "gPodder"
	width: 500
	height: 700
	
	Python {
		Component.onCompleted: {
			addImportPath(dataPath)
		}
	}

	Main {
		scalef: 0.7
	}
}
