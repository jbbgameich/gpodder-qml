#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QUrl>
#include <QStandardPaths>
#include <QDir>
#include <QFontDatabase>

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    app.setApplicationName("gpodder");
    app.setApplicationDisplayName("gPodder");

    QQmlApplicationEngine engine;

    QString path = QStandardPaths::locate(QStandardPaths::DataLocation, "touch",
					  QStandardPaths::LocateDirectory) + QDir::separator() + ".." + QDir::separator();
    QQmlContext* objectContext = engine.rootContext();
    objectContext->setContextProperty("dataPath", path);

    QFontDatabase::addApplicationFont(path + "touch" + QDir::separator() + "icons" +  QDir::separator() + "iconic_fill.ttf");

    engine.load(QUrl(path + QDir::separator() + "main.qml"));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    return app.exec();
}
